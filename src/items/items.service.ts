import { Injectable, NotFoundException } from '@nestjs/common'
import { Item } from '@prisma/client'
import { PrismaService } from '../utils/services/prisma.service'
import { CreateItemDto } from './dtos/create-item.dto'
import { UpdateItemDto } from './dtos/update-item.dto'
import { GetSelectedItemsResponse } from '../utils/types/get-selected-items.response.type'
import { GetSelectedItemsDto } from './dtos/get-selected-items.dto'

@Injectable()
export class ItemsService {
	constructor(private readonly prismaService: PrismaService) {}

	async get(budget: number, userId: number): Promise<Item[]> {
		const items: Item[] = await this.prismaService.item.findMany({
			where: { userId }
		})

		let itemsToReturn: any[] = []

		if (items.length) {
			for (const item of items) {
				itemsToReturn.push({
					...item,
					budgetBeforeProfit: +budget - +item.price,
					budgetAfterProfit: +budget + +item.profit,
					accessable: budget >= item.price
				})
			}
		}

		return itemsToReturn
	}

	async getSelected(
		{ itemIds, budget }: GetSelectedItemsDto,
		userId: number
	): Promise<GetSelectedItemsResponse> {
		let budgetBeforeProfit: number = budget
		let budgetAfterProfit: number = budget
		let profit: number = 0
		let totalPrice: number = 0

		let itemsToReturn: Item[] = []

		if (itemIds.length) {
			for (const itemId of itemIds) {
				const item: Item = await this.prismaService.item.findUnique({
					where: { id: itemId, userId }
				})

				if (!item) {
					throw new NotFoundException()
				}

				budgetBeforeProfit -= item.price
				budgetAfterProfit += item.profit
				profit += item.profit
				totalPrice += item.price

				const itemIdsToReturn = itemsToReturn.map(itemToReturn => itemToReturn.id)

				if (!itemIdsToReturn.includes(item.id)) {
					itemsToReturn.push(item)
				}
			}
		}

		return {
			items: itemsToReturn,
			budgetBeforeProfit,
			budgetAfterProfit: budgetAfterProfit,
			totalProfit: profit,
			totalPrice
		}
	}

	async getOne(id: number, userId: number): Promise<Item> {
		return await this.prismaService.item.findUnique({ where: { id, userId } })
	}

	async createOne(dto: CreateItemDto, userId: number): Promise<Item> {
		const itemProfit = (dto.price * dto.buyer) / 100

		return await this.prismaService.item.create({
			data: {
				name: dto.name,
				price: +dto.price,
				profit: itemProfit,
				buyer: dto.buyer,
				user: {
					connect: {
						id: userId
					}
				}
			}
		})
	}

	async updateOne(id: number, dto: UpdateItemDto, userId: number): Promise<Item> {
		const item: Item | null = await this.getOne(id, userId)

		if (!item) {
			throw new NotFoundException('Item not found')
		}

		const updatedItem = await this.prismaService.item.update({
			where: { id },
			data: dto
		})

		return updatedItem
	}

	async deleteOne(id: number, userId: number): Promise<Item> {
		const item: Item | null = await this.getOne(id, userId)

		if (!item) {
			throw new NotFoundException('Item not found')
		}

		return await this.prismaService.item.delete({
			where: { id }
		})
	}
}
