/*
  Warnings:

  - You are about to drop the column `item_profit` on the `items` table. All the data in the column will be lost.
  - Added the required column `profit` to the `items` table without a default value. This is not possible if the table is not empty.
  - Added the required column `profit_percent` to the `items` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "items" DROP COLUMN "item_profit",
ADD COLUMN     "profit" DOUBLE PRECISION NOT NULL,
ADD COLUMN     "profit_percent" INTEGER NOT NULL;
