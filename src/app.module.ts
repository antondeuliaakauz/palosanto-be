import { Module } from '@nestjs/common'
import { ItemsModule } from './items/items.module'
import { ConfigurationsModule } from './configurations/configurations.module'
import { AuthModule } from './auth/auth.module'
import { UsersModule } from './users/users.module'

@Module({
	imports: [ItemsModule, ConfigurationsModule, AuthModule, UsersModule]
})
export class AppModule {}
