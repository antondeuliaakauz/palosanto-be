/*
  Warnings:

  - You are about to drop the column `itemProfit` on the `items` table. All the data in the column will be lost.
  - Added the required column `item_profit` to the `items` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "items" DROP COLUMN "itemProfit",
ADD COLUMN     "item_profit" DOUBLE PRECISION NOT NULL;

-- CreateTable
CREATE TABLE "currencies" (
    "id" SERIAL NOT NULL,
    "display_name" TEXT NOT NULL,
    "symbol" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "currencies_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "buyers" (
    "id" SERIAL NOT NULL,
    "display_name" TEXT NOT NULL,
    "percent" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "buyers_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "currencies_id_key" ON "currencies"("id");

-- CreateIndex
CREATE UNIQUE INDEX "currencies_display_name_key" ON "currencies"("display_name");

-- CreateIndex
CREATE UNIQUE INDEX "buyers_id_key" ON "buyers"("id");

-- CreateIndex
CREATE UNIQUE INDEX "buyers_display_name_key" ON "buyers"("display_name");
