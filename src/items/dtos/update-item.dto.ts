import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator'

export class UpdateItemDto {
	@IsOptional()
	@IsNotEmpty()
	@IsString()
	readonly name?: string

	@IsOptional()
	@IsNotEmpty()
	@IsNumber()
	readonly price?: number

	@IsOptional()
	@IsNotEmpty()
	@IsNumber()
	readonly amount?: number
}
