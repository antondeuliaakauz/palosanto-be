import { Controller, Get, ParseIntPipe, UseGuards } from '@nestjs/common'
import { UsersService } from './users.service'
import { CurrentUser } from '../utils/decorators/current-user.decorator'
import { JwtGuard } from 'src/auth/guards/jwt.guard'

@UseGuards(JwtGuard)
@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Get()
	async getOne(@CurrentUser('id', ParseIntPipe) userId: number) {
		return await this.usersService.getOne({ id: userId })
	}
}
