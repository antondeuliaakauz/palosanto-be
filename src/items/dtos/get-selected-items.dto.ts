import { IsArray, IsDefined, IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class GetSelectedItemsDto {
	@IsNotEmpty()
	@IsDefined()
	@IsArray()
	readonly itemIds: number[]

	@IsNotEmpty()
	@IsDefined()
	@IsNumber()
	readonly budget: number
}
