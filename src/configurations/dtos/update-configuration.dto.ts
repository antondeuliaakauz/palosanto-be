import { Configuration } from '@prisma/client'

export class UpdateConfigurationDto {
	defaultBudget?: number

	defaultCurrency?: string

	safeDelete?: boolean

	closeAfterCreate?: boolean
}
