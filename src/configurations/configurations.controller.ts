import {
	Body,
	Controller,
	Get,
	Param,
	ParseIntPipe,
	Patch,
	UseGuards
} from '@nestjs/common'
import { ConfigurationsService } from './configurations.service'
import { Configuration } from '@prisma/client'
import { UpdateConfigurationDto } from './dtos/update-configuration.dto'
import { JwtGuard } from '../auth/guards/jwt.guard'
import { CurrentUser } from '../utils/decorators/current-user.decorator'

@UseGuards(JwtGuard)
@Controller('configurations')
export class ConfigurationsController {
	constructor(private readonly configurationsService: ConfigurationsService) {}

	@Get()
	async getOne(@CurrentUser('id', ParseIntPipe) userId: number): Promise<any> {
		return await this.configurationsService.getOne(userId)
	}

	@Patch(':id')
	async updateOne(
		@CurrentUser('id', ParseIntPipe) userId: number,
		@Param('id', ParseIntPipe) id: number,
		@Body() dto: UpdateConfigurationDto
	): Promise<Configuration> {
		return await this.configurationsService.updateOne(id, dto, userId)
	}
}
