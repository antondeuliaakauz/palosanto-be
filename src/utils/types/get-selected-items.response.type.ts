import { Item } from '@prisma/client'

export type GetSelectedItemsResponse = {
	readonly items: Item[]

	readonly totalProfit: number

	readonly budgetBeforeProfit: number

	readonly budgetAfterProfit: number

	readonly totalPrice: number
}
