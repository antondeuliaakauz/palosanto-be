import { Injectable, NotFoundException } from '@nestjs/common'
import { Configuration } from '@prisma/client'
import { PrismaService } from '../utils/services/prisma.service'
import { UpdateConfigurationDto } from './dtos/update-configuration.dto'

@Injectable()
export class ConfigurationsService {
	constructor(private readonly prisma: PrismaService) {}

	async getOne(userId: number): Promise<any> {
		const configuration: Configuration = await this.prisma.configuration.findFirst({
			where: { userId }
		})

		if (configuration) {
			return configuration
		} else {
			return await this.prisma.configuration.create({
				data: { userId }
			})
		}
	}

	async updateOne(id: number, dto: UpdateConfigurationDto, userId: number) {
		const configuration = await this.getOne(userId)

		return await this.prisma.configuration.update({
			where: { id: configuration.id },
			data: dto
		})
	}
}
