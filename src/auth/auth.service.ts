import { Injectable, UnauthorizedException } from '@nestjs/common'
import { UsersService } from '../users/users.service'
import { RegisterDto } from './dtos/register.dto'
import { JwtService } from '@nestjs/jwt'
import { Response } from 'express'
import { JwtTokens } from '../utils/types/jwt-tokens.type'
import { JwtTokensEnum } from '../utils/enums/jwt-tokens.enum'
import * as argon2 from 'argon2'

@Injectable()
export class AuthService {
	constructor(
		private readonly usersService: UsersService,
		private readonly jwtService: JwtService
	) {}

	async register(res: Response, { email, username, password }: RegisterDto) {
		await this.usersService.getOneByEmailOrUsername({
			emailOrUsername: email,
			throwIfExists: true
		})

		await this.usersService.getOneByEmailOrUsername({
			emailOrUsername: username,
			throwIfExists: true
		})

		const hashedPassword = await argon2.hash(password)

		const createdUser = await this.usersService.createOne({
			email,
			username,
			hashedPassword
		})

		const { accessToken, refreshToken }: JwtTokens = await this.generateTokens(
			createdUser.id
		)

		this.setCookies({ res, name: JwtTokensEnum.refreshToken, value: refreshToken })

		return { accessToken }
	}

	async login(res: Response, userId: number) {
		const { accessToken, refreshToken }: JwtTokens = await this.generateTokens(userId)

		this.setCookies({ res, name: JwtTokensEnum.refreshToken, value: refreshToken })

		return { accessToken }
	}

	async validateUser(emailOrUsername: string, password: string) {
		const user = await this.usersService.getOneByEmailOrUsername({
			emailOrUsername,
			returnPassword: true
		})

		if (!user) {
			throw new UnauthorizedException('Invalid credentials')
		}

		const isValidPassword = await argon2.verify(user.hashedPassword, password)

		if (!isValidPassword) {
			throw new UnauthorizedException('Invalid credentials')
		}

		delete user.hashedPassword

		return user
	}

	private async generateTokens(userId: number) {
		const accessToken: string = await this.jwtService.signAsync(
			{ userId },
			{ secret: 'secret', expiresIn: '7d' }
		)

		const refreshToken: string = await this.jwtService.signAsync(
			{ userId },
			{ secret: 'secret', expiresIn: '7d' }
		)

		return {
			accessToken,
			refreshToken
		}
	}

	private setCookies({
		res,
		name,
		value
	}: {
		res: Response
		name: string
		value: string
	}) {
		res.cookie(name, value, {
			httpOnly: true,
			secure: false
		})
	}
}
