import { ConflictException, Injectable, NotFoundException } from '@nestjs/common'
import { PrismaService } from '../utils/services/prisma.service'
import { CreateUserDto } from './dtos/create-user.dto'
import { GetUserDto } from './dtos/get-user.dto'

@Injectable()
export class UsersService {
	constructor(private readonly prisma: PrismaService) {}

	async createOne(dto: CreateUserDto) {
		return await this.prisma.user.create({
			data: dto
		})
	}

	async getOne(dto: GetUserDto) {
		const { returnPassword = false } = dto

		const whereOptions: any = {}

		if (dto.id) {
			whereOptions.id = dto.id
		}

		if (dto.email) {
			whereOptions.email = dto.email
		}

		if (dto.username) {
			whereOptions.username = dto.username
		}

		const user = await this.prisma.user.findFirst({
			where: whereOptions
		})

		if (dto.orThrow && !user) {
			throw new NotFoundException('User not found')
		}

		if (user && !returnPassword) {
			delete user.hashedPassword
		}

		return user
	}

	async getOneByEmailOrUsername({
		emailOrUsername,
		throwIfExists,
		returnPassword = false
	}: {
		emailOrUsername: string
		orThrow?: boolean
		throwIfExists?: boolean
		returnPassword?: boolean
	}) {
		const userByEmail = await this.getOne({ email: emailOrUsername, returnPassword })

		if (throwIfExists && userByEmail) {
			throw new ConflictException('User with this email is already existing')
		}

		const userByUsername = await this.getOne({
			username: emailOrUsername,
			returnPassword
		})

		if (throwIfExists && userByUsername) {
			throw new ConflictException('User with this username is already existing')
		}

		if (userByEmail) {
			return userByEmail
		} else if (userByUsername) {
			return userByUsername
		}
	}
}
