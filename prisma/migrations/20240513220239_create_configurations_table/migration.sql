-- AlterTable
ALTER TABLE "items" ALTER COLUMN "itemProfit" SET DATA TYPE DOUBLE PRECISION;

-- CreateTable
CREATE TABLE "configurations" (
    "id" SERIAL NOT NULL,
    "default_buyer" TEXT NOT NULL,
    "default_currency" TEXT NOT NULL,
    "default_budget" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "configurations_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "configurations_id_key" ON "configurations"("id");
