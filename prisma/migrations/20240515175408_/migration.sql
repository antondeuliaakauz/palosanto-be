/*
  Warnings:

  - You are about to drop the column `buyer_id` on the `configurations` table. All the data in the column will be lost.
  - You are about to drop the column `currency_id` on the `configurations` table. All the data in the column will be lost.
  - You are about to drop the `buyers` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "configurations" DROP CONSTRAINT "configurations_buyer_id_fkey";

-- DropForeignKey
ALTER TABLE "configurations" DROP CONSTRAINT "configurations_currency_id_fkey";

-- AlterTable
ALTER TABLE "configurations" DROP COLUMN "buyer_id",
DROP COLUMN "currency_id",
ADD COLUMN     "default_currency" TEXT NOT NULL DEFAULT '€',
ADD COLUMN     "safe_delete" BOOLEAN NOT NULL DEFAULT true,
ALTER COLUMN "default_budget" SET DEFAULT 0;

-- DropTable
DROP TABLE "buyers";
