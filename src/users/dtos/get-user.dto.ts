export class GetUserDto {
	readonly id?: number

	readonly email?: string

	readonly username?: string

	readonly orThrow?: boolean

	readonly returnPassword?: boolean
}
