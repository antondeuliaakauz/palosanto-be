import { Module } from '@nestjs/common'
import { ConfigurationsService } from './configurations.service'
import { ConfigurationsController } from './configurations.controller'
import { PrismaService } from '../utils/services/prisma.service'

@Module({
	controllers: [ConfigurationsController],
	providers: [ConfigurationsService, PrismaService]
})
export class ConfigurationsModule {}
