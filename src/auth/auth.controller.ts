import {
	Body,
	Controller,
	Delete,
	ParseIntPipe,
	Post,
	Res,
	UseGuards
} from '@nestjs/common'
import { AuthService } from './auth.service'
import { RegisterDto } from './dtos/register.dto'
import { Response } from 'express'
import { CurrentUser } from '../utils/decorators/current-user.decorator'
import { JwtTokensEnum } from '../utils/enums/jwt-tokens.enum'
import { LocalGuard } from './guards/local.guard'

@Controller('auth')
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	@Post('register')
	async register(@Res({ passthrough: true }) res: Response, @Body() dto: RegisterDto) {
		return await this.authService.register(res, dto)
	}

	@UseGuards(LocalGuard)
	@Post('login')
	async login(
		@Res({ passthrough: true }) res: Response,
		@CurrentUser('id', ParseIntPipe) userId: number
	) {
		return await this.authService.login(res, userId)
	}

	@Delete('logout')
	async logout(@Res({ passthrough: true }) res: Response) {
		res.cookie(JwtTokensEnum.refreshToken, '')
	}
}
