import { IsDefined, IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class CreateItemDto {
	@IsNotEmpty()
	@IsDefined()
	@IsString()
	readonly name: string

	@IsNotEmpty()
	@IsDefined()
	@IsNumber()
	readonly price: number

	@IsNotEmpty()
	@IsDefined()
	@IsNumber()
	readonly buyer: number
}
