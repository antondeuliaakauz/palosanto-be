/*
  Warnings:

  - You are about to drop the column `currencyId` on the `items` table. All the data in the column will be lost.
  - You are about to drop the column `mish_buyer` on the `items` table. All the data in the column will be lost.
  - You are about to drop the column `profit_percent` on the `items` table. All the data in the column will be lost.
  - You are about to drop the `currencies` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[user_id]` on the table `configurations` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `user_id` to the `configurations` table without a default value. This is not possible if the table is not empty.
  - Added the required column `buyer` to the `items` table without a default value. This is not possible if the table is not empty.
  - Added the required column `user_id` to the `items` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "items" DROP CONSTRAINT "items_currencyId_fkey";

-- AlterTable
ALTER TABLE "configurations" ADD COLUMN     "user_id" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "items" DROP COLUMN "currencyId",
DROP COLUMN "mish_buyer",
DROP COLUMN "profit_percent",
ADD COLUMN     "buyer" INTEGER NOT NULL,
ADD COLUMN     "user_id" INTEGER NOT NULL;

-- DropTable
DROP TABLE "currencies";

-- CreateTable
CREATE TABLE "users" (
    "id" SERIAL NOT NULL,
    "username" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "hashedPassword" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "users_id_key" ON "users"("id");

-- CreateIndex
CREATE UNIQUE INDEX "users_username_key" ON "users"("username");

-- CreateIndex
CREATE UNIQUE INDEX "users_email_key" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "configurations_user_id_key" ON "configurations"("user_id");

-- AddForeignKey
ALTER TABLE "items" ADD CONSTRAINT "items_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "configurations" ADD CONSTRAINT "configurations_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;
