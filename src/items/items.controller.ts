import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	ParseIntPipe,
	Patch,
	Post,
	Query,
	UseGuards
} from '@nestjs/common'
import { ItemsService } from './items.service'
import { Item } from '@prisma/client'
import { CreateItemDto } from './dtos/create-item.dto'
import { UpdateItemDto } from './dtos/update-item.dto'
import { GetSelectedItemsResponse } from '../utils/types/get-selected-items.response.type'
import { GetSelectedItemsDto } from './dtos/get-selected-items.dto'
import { JwtGuard } from '../auth/guards/jwt.guard'
import { CurrentUser } from '../utils/decorators/current-user.decorator'

@UseGuards(JwtGuard)
@Controller('items')
export class ItemsController {
	constructor(private readonly itemsService: ItemsService) {}

	@Patch(':id')
	async updateOne(
		@CurrentUser('id', ParseIntPipe) userId: number,
		@Param('id', ParseIntPipe) id: number,
		@Body() dto: UpdateItemDto
	): Promise<Item> {
		return await this.itemsService.updateOne(id, dto, userId)
	}

	@Get()
	async get(
		@CurrentUser('id', ParseIntPipe) userId: number,
		@Query('budget') budget: number
	): Promise<Item[]> {
		return await this.itemsService.get(budget, userId)
	}

	@Post('selected')
	async getSelected(
		@CurrentUser('id', ParseIntPipe) userId: number,
		@Body() dto: GetSelectedItemsDto
	): Promise<GetSelectedItemsResponse> {
		return await this.itemsService.getSelected(dto, userId)
	}

	@Post()
	async createOne(
		@CurrentUser('id', ParseIntPipe) userId: number,
		@Body() dto: CreateItemDto
	): Promise<any> {
		return await this.itemsService.createOne(dto, userId)
	}

	@Delete(':id')
	async deleteOne(
		@CurrentUser('id', ParseIntPipe) userId: number,
		@Param('id', ParseIntPipe) id: number
	): Promise<Item> {
		return await this.itemsService.deleteOne(id, userId)
	}
}
