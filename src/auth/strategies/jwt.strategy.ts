import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { UsersService } from '../../users/users.service'
import { JwtPayload } from '../../utils/types/jwt-payload.type'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private readonly usersService: UsersService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: 'secret'
		})
	}

	async validate({ userId }: JwtPayload) {
		const user = await this.usersService.getOne({ id: userId })

		if (!user) {
			throw new UnauthorizedException('User not found')
		}

		return user
	}
}
