/*
  Warnings:

  - You are about to drop the column `default_buyer` on the `configurations` table. All the data in the column will be lost.
  - You are about to drop the column `default_currency` on the `configurations` table. All the data in the column will be lost.
  - Added the required column `buyer_id` to the `configurations` table without a default value. This is not possible if the table is not empty.
  - Added the required column `currency_id` to the `configurations` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "configurations" DROP COLUMN "default_buyer",
DROP COLUMN "default_currency",
ADD COLUMN     "buyer_id" INTEGER NOT NULL,
ADD COLUMN     "currency_id" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "configurations" ADD CONSTRAINT "configurations_buyer_id_fkey" FOREIGN KEY ("buyer_id") REFERENCES "buyers"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "configurations" ADD CONSTRAINT "configurations_currency_id_fkey" FOREIGN KEY ("currency_id") REFERENCES "currencies"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
