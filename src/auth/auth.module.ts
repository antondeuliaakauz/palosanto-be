import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { AuthController } from './auth.controller'
import { UsersModule } from '../users/users.module'
import { JwtModule } from '@nestjs/jwt'
import { JwtStrategy } from './strategies/jwt.strategy'
import { LocalStrategy } from './strategies/local.strategy'
import { JwtGuard } from './guards/jwt.guard'
import { LocalGuard } from './guards/local.guard'

@Module({
	imports: [
		JwtModule.register({
			secret: 'secret',
			signOptions: {
				expiresIn: '7d'
			}
		}),
		UsersModule
	],
	controllers: [AuthController],
	providers: [AuthService, JwtStrategy, LocalStrategy, JwtGuard, LocalGuard]
})
export class AuthModule {}
